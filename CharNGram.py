#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  CharacterNgram.py
#  
#  Copyright 2013 Phiradet Bangcharoensap <phiradet@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import codecs
import Preprocessing

class NGram:
	
	def __init__(self, N=3, min_df=1, token_pattern='.', vocabulary=None, useIdf=True, norm=None):
		self.N = N
		if vocabulary==None:
			if useIdf:
				self.vectorizer = TfidfVectorizer(ngram_range=(N, N), token_pattern=token_pattern, min_df=min_df, norm=norm)
			else:
				self.vectorizer = CountVectorizer(ngram_range=(N, N), token_pattern=token_pattern, min_df=min_df)
		else:
			if useIdf:
				self.vectorizer = TfidfVectorizer(ngram_range=(N, N), token_pattern=token_pattern, min_df=min_df, vocabulary=vocabulary, norm=norm)
			else:
				self.vectorizer = CountVectorizer(ngram_range=(N, N), token_pattern=token_pattern, min_df=min_df, vocabulary=vocabulary)
	
	def NegationReplace(self, inStr):
		return inStr.replace('_NEG','§')
	
	def SpaceReplace(self, inStr):
		return inStr.replace(' ','¶')
	
	def Preprocess(self, sentence):
		return self.NegationReplace(self.SpaceReplace(sentence))
	
	def CreateNGramMatrix(self, corpus):
		corpus = map(self.Preprocess, corpus)
		self.mat = self.vectorizer.fit_transform(corpus).toarray()
		return self.mat
	
	def GetVocabularyList(self):
		vocabularies = self.vectorizer.vocabulary_.items()
		vocabularies.sort(key=lambda x:x[1])
		return map(lambda x:x[0], vocabularies)
	
	def GetTermFreq(self):
		return np.array(self.mat).sum(axis=0)
	
	def MakeWordFreqReport(self, reportPath=None):
		if reportPath==None:
			reportPath = "char%dGramReport.csv"%(self.N)
		vocabularies = self.GetVocabularyList()
		termFreq = self.GetTermFreq()
		
		fOut = codecs.open(reportPath,'w', 'utf-8')
		for i in range(len(termFreq)):
			fOut.write('"%s"\t%s\n'%(vocabularies[i], termFreq[i]))

def ReadCorpus(corpusPath):
	lines = codecs.open(corpusPath,'r')
	allTweet = []
	allTweetID = []
	for line in lines:
		line = line.strip()
		parts = line.split('\t')
		tweetID = parts[0]
		tweetMsg = parts[3]
		allTweetID.append(tweetID)
		allTweet.append(tweetMsg)
	return allTweet, allTweetID

def LoadVocabulary(refPath):
	f = open(refPath,'r')
	index = 0
	vocab = {}
	for line in f.readlines():
		parts = line.split('\t')
		gram = parts[0]
		vocab[gram]=index
		index+=1
		print index
	return vocab

def PrintGramMatrix(mat, tweetID, outPath):
	f = open(outPath, 'w')
	i = 0
	for r in mat:
		f.write("%s"%(tweetID[i]))
		i+=1
		for c in r:
			isPresence = 1 if c>0 else 0
			f.write("\t%d"%(isPresence))
		f.write("\n")

def TriGramTrainFeatureExtraction():
	vocab = LoadVocabulary('./Corpus/3Gram.list')
	print len(vocab)
	corpus, ids = ReadCorpus('./Corpus/tweet-b.dist.tokenized_neg')
	n = NGram(N=3, useIdf=False, vocabulary=vocab)
	mat = n.CreateNGramMatrix(corpus)
	print len(mat), len(mat[0])
	PrintGramMatrix(mat, ids, './Corpus/3GramChar.train.feature')


def QuardGramTrainFeatureExtraction():
	vocab = LoadVocabulary('./Corpus/4Gram.list')
	print len(vocab)
	corpus, ids = ReadCorpus('./Corpus/tweet-b.dist.tokenized_neg')
	n = NGram(N=4, useIdf=False, vocabulary=vocab)
	mat = n.CreateNGramMatrix(corpus)
	print len(mat), len(mat[0])
	PrintGramMatrix(mat, ids, './Corpus/4GramChar.train.feature')

def TriGramDevFeatureExtraction():
	vocab = LoadVocabulary('./Corpus/3Gram.list')
	print len(vocab)
	corpus, ids = ReadCorpus('./Corpus/tweet-b.dev.tokenized_neg')
	n = NGram(N=3, useIdf=False, vocabulary=vocab)
	mat = n.CreateNGramMatrix(corpus)
	print len(mat), len(mat[0])
	PrintGramMatrix(mat, ids, './Corpus/3GramChar.dev.feature')


def QuardGramDevFeatureExtraction():
	vocab = LoadVocabulary('./Corpus/4Gram.list')
	print len(vocab)
	corpus, ids = ReadCorpus('./Corpus/tweet-b.dev.tokenized_neg')
	n = NGram(N=4, useIdf=False, vocabulary=vocab)
	mat = n.CreateNGramMatrix(corpus)
	print len(mat), len(mat[0])
	PrintGramMatrix(mat, ids, './Corpus/4GramChar.dev.feature')

def main():
	QuardGramTrainFeatureExtraction()
	QuardGramDevFeatureExtraction()
	TriGramTrainFeatureExtraction()
	TriGramDevFeatureExtraction()
	return 0

if __name__ == '__main__':
	main()
