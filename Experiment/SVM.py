#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  SVM.py
#  
#  Copyright 2013 Phiradet Bangcharoensap <phiradet@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from sklearn import svm
import numpy as np
import sys
from sklearn import metrics
from sklearn import cross_validation

def ReadFeatureVector(path):
	f = open(path)
	numerizer = lambda x:float(x.split(':')[1])
	featureVector = []
	label = []
	for line in f:
		parts = line.strip().split(' ')
		currLabel = int(parts[0])
		label.append(currLabel)
		currVector = map(numerizer, parts[1:])
		featureVector.append(currVector)
	return label, featureVector 

def Evaluation(actual, predict):
	TP = TN = FP = FN = 0
	for i in range(len(actual)):
		p = predict[i]
		a = actual[i]
		print a,p,int(bool(p==a))
		if p==1:
			if p==a:
				TP+=1
			else:
				FP+=1
		else:
			if p==a:
				TN+=1
			else:
				FN+=1
	print "TP FP FN TN"
	print TP, FP, FN, TN
	precision = float(TP)/(TP+FP)
	recall = float(TP)/(TP+FN)
	print "Precision", precision
	print "Recall", recall
	print "f-measure", 2*precision*recall/(precision+recall)

def SVM(trainDataPath, testDataPath):
	labelTrain, vectorTrain = ReadFeatureVector(trainDataPath)
	labelTest, vectorTest = ReadFeatureVector(testDataPath)
	clf = svm.SVC(C=0.005, kernel='linear')
	clf.fit(vectorTrain, labelTrain)
	labelPredict = clf.predict(vectorTest)
	
	Evaluation(labelTest, labelPredict)
	return 0


def CrossValidation(trainDataPath):
	labelTrain, vectorTrain = ReadFeatureVector(trainDataPath)
	clf = svm.SVC(C=0.005, kernel='linear')
	#print vectorTrain[1][:15]
	#print '----'
	#print labelTrain[:10]
	scores = cross_validation.cross_val_score(clf, np.array(vectorTrain), np.array(labelTrain), cv=10, scoring='f1')
	for s in scores:
		print s

if __name__ == '__main__':
	CrossValidation(sys.argv[1])
	#SVM(sys.argv[1],sys.argv[2])
