#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2013 Phiradet Bangcharoensap <phiradet@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
 

from lab_SVM_POS_allCap import CMU_POS

import sys

def main():
	filename = sys.argv[1]
	f = open(filename, 'r')
	prefix = []
	rawSuffix = []
	tokenizedSuffix = []
	for line in f:
		parts = line.strip().split('\t')
		prefix.append('\t'.join(parts[:3]))
		rawSuffix.append(parts[3])
	a = CMU_POS()
	tokenizedSuffix = a.Tokenize(rawSuffix)
	fOut = open(filename+'.tokenized','w')
	for i in range(len(prefix)):
		fOut.write("%s\t%s\n"%(prefix[i],'¤'.join(tokenizedSuffix[i])))
		
	return 0

if __name__ == '__main__':
	main()

