#!/bin/bash

python ../../../GenFeatureVector.py -o Feature.train -l label.train AllCap_norm.train.feature cluster_features.train.txt emoticon_features.train.txt POS_norm.train.feature punc_elong_neg_norm.train.features capitalized.lex.train_norm 1gram.lex.train_norm 2gram.lex.train_norm hashtag.lex.train_norm 3GramChar.train.feature 4GramChar.train.feature 1Gram.train 2Gram.train
