#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2013 Phiradet Bangcharoensap <phiradet@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys, getopt
import numpy as np


def ExtractLabel(inFile=None, outFile=None):
	fIn = None
	if inFile==None:
		fIn = sys.stdin
	else:
		fIn = open(inFile, 'r')
	
	fOut = None
	if outFile == None:
		fOut = sys.stdout
	else:
		fOut = open(outFile, 'w')
	
	for line in fIn:
		line = line.strip()
		if not line:
			continue
		parts = line.split('\t')
		tweetID = parts[0]
		label = parts[2]
		if label=='"positive"':
			fOut.write("%s\t%s\n"%(tweetID, '+1'))
		elif label == '"negative"':
			fOut.write("%s\t%s\n"%(tweetID, '-1'))
		else:
			fOut.write("%s\t%s\n"%(tweetID, '0'))

def AddFeatureIndex(vector, prevIndex):
	output = ""
	index = prevIndex
	for i in vector:
		index = index+1
		output += "\t%d:%s"%(index, i)
	return output, index

def isInconsistence(labels):
	if '-1' in labels and '+1' in labels:
		return True
	if '-1' not in labels and '+1' not in labels:
		return True
	return False

def GetInconsistencyTweetID(refFile):
	f = open(refFile, 'r')
	tweetRecord = {}
	for line in f:
		parts = line.strip().split('\t')
		key = parts[0]
		label = parts[1]
		if key not in tweetRecord.keys():
			tweetRecord[key] = []
		tweetRecord[key].append(label)
		
	output = []
	for k,v in tweetRecord.items():	
		if isInconsistence(v):
			output.append(k)
	return output
	
def MergeFeatureVector(argv):
	try:
		opts, args = getopt.getopt(argv, "o:l:",["output=","label="])
	except getopt.GetoptError:
		print "python GatherFeatureVector.py -o <output> <in0> <in1> ..."
		exit(2)
	outputPath = ''
	labelPath = ''
	for opt,arg in opts:
		if opt in ["-o", "--output"]:
			outputPath = arg
		elif opt in ["-l", "--label"]:
			labelPath = arg
		else:
			print "Cannot recongnize option %s %s"%(opt, arg)
	if outputPath=='':
		outputPath = 'output.matrix'
		print 'output filename=./output.matrix'
	outputFObj = open(outputPath, 'w')
	fObjs = []
	if len(args)==0:
		print "Please specify input feature vector"
		exit(2)
	print "Input:", '-'.join(args)
	print len(args)
	for inFileName in args:
		f = open(inFileName, 'r')
		fObjs.append(f)
	
	inconsistencyID = GetInconsistencyTweetID(labelPath)
	
	featureRecord = {}
	idList = []
	fLabel = open(labelPath)
	for line in fLabel:
		parts = line.strip().split('\t')
		Id = parts[0]
		label = parts[1]
		if label in ["-1", "+1"] and Id not in inconsistencyID:
			idList.append(Id)
			if Id not in featureRecord.keys():
				featureRecord[Id] = [] 
				featureRecord[Id].append(label)
			else:
				if featureRecord[Id][0]!=label:
					print 'alert duplicate', Id
					exit(2)
	
	print 'file', len(fObjs)
	for i in range(len(fObjs)):
		for line in fObjs[i]:
			parts = line.strip().split('\t')
			currID = parts[0]
			featureVector = parts[1:]
			if currID in idList:
				if len(featureRecord[currID]) == i+1:
					featureRecord[currID].append(featureVector)
	
	prevFeatureCounter = 0
	linecount = 0
	for currId in idList:
		currFeatureVector = featureRecord[currId]
		outputFObj.write(currFeatureVector[0])
		featureCounter = 1
		#print '\n',len(currFeatureVector[1:])
		for fSet in currFeatureVector[1:]:
			#print len(fSet),' ',
			for val in fSet:
				outputFObj.write(" %d:%s"%(featureCounter,val))
				featureCounter+=1
		if prevFeatureCounter>0 and prevFeatureCounter!=featureCounter:
			print 'this id violate square', currId
		prevFeatureCounter = featureCounter
		outputFObj.write('\n')
		linecount+=1		
	return 0
		
#~ def MergeFeatureVector(argv):
	#~ try:
		#~ opts, args = getopt.getopt(argv, "o:",["output="])
	#~ except getopt.GetoptError:
		#~ print "python GatherFeatureVector.py -o <output> <in0> <in1> ..."
		#~ exit(2)
	#~ outputPath = ''
	#~ for opt,arg in opts:
		#~ if opt in ["-o", "--output"]:
			#~ outputPath = arg
		#~ else:
			#~ print "Cannot recongnize option %s %s"%(opt, arg)
	#~ if outputPath=='':
		#~ outputPath = 'output.matrix'
		#~ print 'output filename=./output.matrix'
	#~ outputFObj = open(outputPath, 'w')
	#~ fObjs = []
	#~ if len(args)==0:
		#~ print "Please specify input feature vector"
		#~ exit(2)
	#~ print "Input:", '-'.join(args)
	#~ for inFileName in args:
		#~ f = open(inFileName, 'r')
		#~ fObjs.append(f)
	#~ 
	#~ counter = 0
	#~ featureIndex = 0
	#~ currLine = ""
	#~ prevID = ""
	#~ isWillWrite = True
	#~ linenumIn = 0
	#~ linenumOut = 0
	#~ 
	#~ while True:
	#~ 
		#~ currFileObj = fObjs[counter] #Update file pointer to next file
#~ 
		#~ line = currFileObj.readline()
		#~ if not line:	#If EOF, then break
			#~ break
#~ 
		#~ parts = line.strip().split('\t')
		#~ 
		#~ if counter==0: #if this iteration is the new row
			#~ currLine = ""	 #reset the accumulate feature vector string
			#~ isWillWrite = True #reset the label tag
			#~ featureIndex = 0
			#~ linenumIn += 1
			#~ if parts[1]=='0':
				#~ isWillWrite=False
#~ 
		#~ if counter>0 and prevID != parts[0] and isWillWrite: #Is current tweet ID consistence with the previous file 
			#~ print 'Incompatible found in line %d of "%s" (ID %s VS ID %s) with file "%s"'%(linenumIn, args[counter], parts[0], prevID, args[counter-1])
			#~ exit(2)
		#~ prevID = parts[0]
		#~ 
		#~ vector = []
		#~ if counter>0:
			#~ vector,featureIndex = AddFeatureIndex(parts[1:], featureIndex) #Feature vector of the present file
		#~ else:
			#~ vector = parts[1]
#~ 
		#~ currLine += vector
		#~ if counter == len(fObjs)-1: #If this is the last file object
			#~ if isWillWrite:
				#~ linenumOut += 1
				#~ print linenumOut, len(currLine)
				#~ outputFObj.write("%s\n"%currLine) #Write file to destination
#~ 
		#~ counter +=1
		#~ counter = counter%len(fObjs)
	#~ return 0

def Normalize(value, maxVal, minVal):
	if maxVal-minVal==0:
		return value
	return float(value-minVal)/float(maxVal-minVal)

def NormalizeFeatureVector(vector):
	npVector = np.array(vector)
	rowNum, colNum = npVector.shape
	colMax = npVector.max(axis=0)
	colMin = npVector.min(axis=0)
	
	for r in range(rowNum):
		for c in range(colNum):
			normVal = Normalize(npVector[r][c], colMax[c], colMin[c])
			npVector[r][c] = normVal
	return npVector

def NormalizeFromFile(inPath, outPath):
	f = open(inPath)
	allTweetId = []
	featureVector = []
	for line in f:
		parts = line.split('\t')
		allTweetId.append(parts[0])
		featureVector.append(map(lambda x:float(x),parts[1:]))
	normFeatureVector = NormalizeFeatureVector(featureVector)
	f.close()
	f = open(outPath, 'w')
	for r in range(len(normFeatureVector)):
		f.write('%s'%(allTweetId[r]))
		for c in range(len(normFeatureVector[r])):
			f.write('\t%f'%(normFeatureVector[r][c]))
		f.write('\n')	

def NormAllFile():
	import glob
	allFile = glob.glob('/home/phib/Downloads/lexicon/datafrom/*.*')
	for f in allFile:
		NormalizeFromFile(f,f+'_norm')
	#~ #------ DEV ------------
	#~ NormalizeFromFile('./Corpus/FeatureSet/Dev/punc_elong_neg.dev.features', './Corpus/FeatureSet/Dev/punc_elong_neg_norm.dev.features')
	#~ NormalizeFromFile('./Corpus/FeatureSet/Dev/AllCap.dev.feature', './Corpus/FeatureSet/Dev/AllCap_norm.dev.feature')
	#~ NormalizeFromFile('./Corpus/FeatureSet/Dev/POS.dev.feature', './Corpus/FeatureSet/Dev/POS_norm.dev.feature')
	#~ 
	#~ #------ TRAIN ----------
	#~ NormalizeFromFile('./Corpus/FeatureSet/Train/punc_elong_neg.train.features', './Corpus/FeatureSet/Train/punc_elong_neg_norm.train.features')
	#~ NormalizeFromFile('./Corpus/FeatureSet/Train/AllCap.train.feature', './Corpus/FeatureSet/Train/AllCap_norm.train.feature')
	#~ NormalizeFromFile('./Corpus/FeatureSet/Train/POS.train.feature', './Corpus/FeatureSet/Train/POS_norm.train.feature')

if __name__ == '__main__':
	#NormAllFile()
	#ExtractLabel('./Corpus/tweet-b.dist','./Corpus/FeatureSet/Train/label.train')
	#ExtractLabel('./Corpus/tweet-b.dev','./Corpus/FeatureSet/Dev/label.dev')
	MergeFeatureVector(sys.argv[1:])
	#GetInconsistencyTweetID('/home/phib/Dropbox/MasterStudy/HumanCentered/Code/Corpus/FeatureSet/Train/label.train')

