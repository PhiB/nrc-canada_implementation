#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  lab_SVM_POS_allCap.py
#  
#  Copyright 2013 Phiradet Bangcharoensap <phiradet@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import subprocess, os, sys

class CMU_POS:
	
	def __init__(self):
		self.POStagCmd = ""
		self.TokenizeCmd = ""
		if os.name=='posix':
			self.POStagCmd= "./lib/ark-tweet-nlp-0.3.2/runTagger.sh --output-format conll"
			self.TokenizeCmd= "./lib/ark-tweet-nlp-0.3.2/tokenize.sh"
		else:
			self.POStagCmd = r"java -XX:ParallelGCThreads=2 -Xmx500m -jar .\lib\ark-tweet-nlp-0.3.2\ark-tweet-nlp-0.3.2.jar --output-format conll"
			self.TokenizeCmd = r"java -XX:ParallelGCThreads=2 -Xmx500m -jar .\lib\ark-tweet-nlp-0.3.2\ark-tweet-nlp-0.3.2.jar --output-format conll --just-tokenize"
		print self.POStagCmd
		print self.TokenizeCmd
		#self.POStagProcess = subprocess.Popen(self.POStagCmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		#self.TokenizeProcess = subprocess.Popen(self.TokenizeCmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)

		self.allPOS = ['N','O','^','S','Z','V','A','R','!','D','P','&','T','X','#','@','~','U','E','$',',','G','L','M','Y']
		self.POSkey = {}
		for i in range(len(self.allPOS)):
			self.POSkey[self.allPOS[i]] = i
	
	def POStagging(self, sentences):
		joinedSentence = '\n'.join(sentences)
		
		self.POStagProcess = subprocess.Popen(self.POStagCmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		outputStr, error = self.POStagProcess.communicate(joinedSentence)
		
		output = []
		currSentenceOutput = []
		outputStr = outputStr.strip()
		for line in outputStr.split('\n'):
			line = line.strip()
			if line != "":
				parts = line.split('\t')
				tokens = parts[0].split()
				tags = parts[1].split()
				for tok, tag in zip(tokens, tags):
					currSentenceOutput.append((tok, tag))
			else:
				output.append(currSentenceOutput)
				currSentenceOutput = []
		output.append(currSentenceOutput) 
		return output
	
	def Tokenize(self, sentences):
		joinedSentence = '\n'.join(sentences)
		
		self.TokenizeProcess = subprocess.Popen(self.TokenizeCmd, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		outputStr, error = self.TokenizeProcess.communicate(joinedSentence)
		outputStr= outputStr.strip()
		#print 'OUT::',outputStr
		output = []
		currOutput = []
		
		for line in outputStr.split('\n'):
			line = line.strip()
			
			parts = line.split('\t')
			tokenizedStr = parts[0]
			inputStr = parts[1]
			
			output.append(tokenizedStr.split(' '))
		return output
	
	def GetPOSFeature(self, samples):
		POStags = self.POStagging(samples)
		featureTable = []
		for tagPair in POStags:
			currRow = [0]*len(self.allPOS)
			for token, tag in tagPair:
				currRow[self.POSkey[tag]]+=1
			featureTable.append(currRow)
		return featureTable

def IsAllCap(strIn):
	for c in strIn:
		if not c.isupper():
			return False
	return True
			
				
def CountAllCap(tokennizedSents):
	output = []
	for sentence in tokennizedSents:
		count = 0
		for token in sentence:
			if IsAllCap(token):
				count+=1
				#print token
		output.append(count)
	return output

def CreateAllCapFeatureVector(inFile, outFile=None, isUseTweetID=True):
	import logging, sys
	logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
	
	lines = open(inFile,'r')
	lineNum = 1
	allTweet = []
	allTweetID = []
	for line in lines:
		line = line.strip()
		parts = line.split('\t')
		tweetID = parts[0]
		tweetMsg = parts[3]
		allTweetID.append(tweetID)
		allTweet.append(tweetMsg)
	
	a = CMU_POS()
	logging.debug("Finished Loaded")
	tokenizedTweets = a.Tokenize(allTweet)
	logging.debug("Finished Tokenize")
	allCapCount = CountAllCap(tokenizedTweets)
	logging.debug("Finished AllCap")
	
	output = []
	f = None
	if outFile!=None:
		f=open(outFile, 'w')
	logging.debug("Start output")
	
	for i in range(len(allTweet)):
		#currFeatureVector = [allCapCount[i]]+POSFeatureVector[i]
		currFeatureVector = [allCapCount[i]]
		if outFile==None:
			output.append(currFeatureVector)
		else:
			if isUseTweetID:
				f.write('\t'.join([allTweetID[i]]+map(str,currFeatureVector))+'\n')
			else:
				f.write('\t'.join([str(i+1)]+map(str,currFeatureVector))+'\n')
	f.close()
	if outFile==None:
		return output

def CreatePOSFeatureVector(inFile, outFile=None, isUseTweetID=True):
	import logging, sys
	logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
	
	a = CMU_POS()
	lines = open(inFile,'r')
	lineNum = 1
	allTweet = []
	allTweetID = []
	for line in lines:
		line = line.strip()
		parts = line.split('\t')
		tweetID = parts[0]
		tweetMsg = parts[3]
		allTweetID.append(tweetID)
		allTweet.append(tweetMsg)
	
	logging.debug("Finished Loaded")
	POSFeatureVector = a.GetPOSFeature(allTweet)
	logging.debug("Finished POS Tagging")
	tokenizedTweets = a.Tokenize(allTweet)
	logging.debug("Finished Tokenize")
	#allCapCount = CountAllCap(tokenizedTweets)
	#logging.debug("Finished AllCap")
	
	output = []
	f = None
	if outFile!=None:
		f=open(outFile, 'w')
	logging.debug("Start output")
	
	for i in range(len(allTweet)):
		#currFeatureVector = [allCapCount[i]]+POSFeatureVector[i]
		currFeatureVector = POSFeatureVector[i]
		if outFile==None:
			output.append(currFeatureVector)
		else:
			if isUseTweetID:
				f.write('\t'.join([allTweetID[i]]+map(str,currFeatureVector))+'\n')
			else:
				f.write('\t'.join([str(i+1)]+map(str,currFeatureVector))+'\n')
	f.close()
	if outFile==None:
		return output

def main():
	a = CMU_POS()
	#print a.POStagging(["I love NLP #toomuch", "I love TIECH :) ^_^"])
	#print '----'
	#print ' '.join(a.allPOS)
	#print a.GetPOSFeature(["I love NLP #toomuch", "I love TIECH :) ^_^"])
	#print CountAllCap(a.Tokenize(["I love NLP #toomuch", "I love TIECH :) ^_^"]))
	print '----',a.Tokenize(["I love NLP, #toomuch", "I love TIECH :) ^_^"])
	return 0

if __name__ == '__main__':
	#~ CreatePOSFeatureVector('./tweet-b.dist','./Corpus/POS.train.feature')
	#~ CreateAllCapFeatureVector('./tweet-b.dist','./Corpus/AllCap.train.feature')
	CreatePOSFeatureVector('./Corpus/tweet-b.dev.dist','./Corpus/POS.dev.feature')
	CreateAllCapFeatureVector('./Corpus/tweet-b.dev.dist','./Corpus/AllCap.dev.feature')
	#main()

